<?php

define('PUZZLES_CACHE', 'puzzles.cache');
define('PUZZLES_DIR', 'puzzles');


function custom_hash($s) {
  $h = 0;
  $len = strlen($s);
  for ($i = 0; $i < $len; $i++) {
    $c = substr($s, $i, 1);
    $h = ($h<<5)-$h+ord($c);
  }
  return $h;
}

function load_puzzle($path)
{
  $contents = file_get_contents($path);
  $puzzle = json_decode($contents, true);
  $grid = $puzzle['grid'];

  $infos['title'] = $puzzle['title'];
  $infos['difficulty'] = $puzzle['difficulty'];
  $infos['width'] = count($grid[0]) ;
  $infos['height'] = count($grid);
  $infos['hash'] = custom_hash(json_encode($grid));
  return $infos;
}


function update_dir($dirpath, $cache)
{
  $puzzles = Array();
  $changes = false;

  // Iterate through the puzzle directory
  $directory = new DirectoryIterator($dirpath);
  foreach ($directory as $fileinfo) {
    $path = $fileinfo->getPathname();
    $mtime = $fileinfo->getMTime();

    // If the file is a puzzle
    if ($fileinfo->isFile() && $fileinfo->getExtension() == 'json') {
      // Is the file in cache and up to date ?
      if (isset($cache[$path]) && $cache[$path]['last_update'] == $mtime) {
        // Use cache
        $puzzles[$path] = $cache[$path];
      }
      else {
        // Update puzzle information
        $puzzles[$path] = load_puzzle($path);
        $puzzles[$path]['last_update'] = $mtime;
        $changes = true;
      }
    }

    if ($fileinfo->isDir() && !$fileinfo->isDot()) {
      $dircache = isset($cache[$path]) ? $cache[$path] : Array();
      list($puzzles[$path], $dirchanges) = update_dir($path, $dircache);
      $changes = $changes || $dirchanges;
    }
  }

  return Array($puzzles, $changes);
}

function update_list()
{
  // Load cache if it exists
  if (file_exists(PUZZLES_CACHE))
    $cache = unserialize(file_get_contents(PUZZLES_CACHE));
  if (!isset($cache) || !$cache)
    $cache = Array();

  list($puzzles, $changes) = update_dir(PUZZLES_DIR, $cache);

  // Update cache if it changes or if some puzzles have been removed
  if ($changes || count($puzzles) != count($cache))
    file_put_contents(PUZZLES_CACHE, serialize($puzzles));

  return $puzzles;
}

$puzzles = update_list();
header('Content-type: application/json');
echo json_encode($puzzles);

?>
