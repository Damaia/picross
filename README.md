# Picross

Picross is a simple logic game. It is composed of a grid and hints. The goal of the game is to fill the grid with the right colors, with the help of the hints. The final result is usually a drawing.

## Getting started

Download it and put it on your distant or local server. You may also just open index.html in your browser, it will work as well. However, be aware that with this solution, you won't be able to access any puzzle except for the default one.

## Rules

- There is a set of hints for each color. It corresponds to the number and size of the color blocks on each corresponding line or column.
- You can cross the tiles you know are not of the chosen color.
- No tile must remain blank. No tile has several color, neither.
- The puzzle should be possible to solve without any guessing. If you need to do a guess to solve a puzzle, either you did something wrong before, either the puzzle is flawed. In this case, please contact me or the author of the puzzle for a fix.

## Commands

The game is played with the mouse only.
- A right click on a blank tile puts a cross of the selected color on it.
- A left click on a blank tile fills it with the selected color.
- A left of right click on a non-blank tile erases what was on it.
- A left click on a color on the color picker beside the grid changes the selected color to the color clicked.
- A right click on a line or column fills all the blank tiles of the line or column clicked with crosses of the selected color.
- A left click on a line or column fills all the blank tiles of the line or column clicked with the selected color.
- You can use drag and drop with either right or left click to fill as many tiles as you wish to. Please not the drag and drop will only do the same thing as what was done on the first tile : fill, cross, or erase.

## Hints

The purpose of the hints is to guide you through the puzzle. On this version they are dynamic, meaning that they will adapt to the tiles you filled or crossed.

The color of the hint indicates its state. A grey hint means that the corresponding block was filled. A red hint means that there is a conflict in the puzzle's blocks. A black hint indicates a block that wasn't filled yet.

The state of each color appears on the color picker beside the grid. A red cross will show up on the corresponding color if there is a conflict on this color. A green "V" will show up if each color block is valid on the corresponding color.

## Adding puzzles

The puzzles are JSON files. I provided several examples in the puzzles/ folder. If you want to add a puzzle, just write it down in JSON and then put it inside the puzzles/ folder. It should appear on the right of the window once the page is reloaded.

To build a puzzle, you simply have to write an array of hexadecimal colors, corresponding to the different colored tiles which make the puzzle.
