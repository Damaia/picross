var difficulties = {
  "very easy": 4,
  "easy": 6,
  "medium": 10,
  "hard": 20,
  "very hard": 40,
  "godly": 1000000
}

function loadPuzzle(file, onload) {
  var ext = file.name.split('.').pop()
  if (ext == 'bmp') {
    importBitmap(file, {}, onload)
  }
  else {
    var reader = new FileReader()
    reader.addEventListener("load", function () {
        onload(JSON.parse(this.result))
      }, false)
    reader.readAsText(file)
  }
}

function importBitmap(file, infos, onload) {
  var name = file.name.substring(0, file.name.indexOf('.'))
  loadBitmap(file, function (bitmap) {
    var puzzle = {
        'title' : infos['title'] ? infos['title'] : name,
        'original-title' : infos['original-title'],
        'author' : infos['author'] ? infos['author'] : 'unknown',
        'author-url' : infos['author-url'],
        'source' : infos['source'],
        'licence' : infos['licence'],
        'notice' : infos['notice'],
        difficulty: 'unrated',
        grid: bitmap
      }
    setDifficulty(puzzle, onload)
  })
}

function setDifficulty(puzzle, cont)
{
  var game = new Game(puzzle)
  var s = solve(game)
  var solve_timer = setInterval(function() {
    var p = s.next()
    if (p.done) {
      clearInterval(solve_timer)
      puzzle.difficulty = evaluateDifficulty(p.value)
      output('Difficulty set to "' + puzzle.difficulty + '"')
      cont(puzzle)
    }
  }, 0)
}

function evaluateDifficulty(iterations) {
  if (iterations == -1)
    return "impossible"
  for (var difficulty in difficulties) {
    if (iterations <=  difficulties[difficulty])
      return difficulty
  }
}

function exportPuzzle(puzzle) {
  return '{\n' +
    '  "title":' + JSON.stringify(puzzle['title']) + ',\n' +
    '  "original-title":' + JSON.stringify(puzzle['original-title']) + ',\n' +
    '  "author":' + JSON.stringify(puzzle['author']) + ',\n' +
    '  "author-url":' + JSON.stringify(puzzle['author-url']) + ',\n' +
    '  "source":' + JSON.stringify(puzzle['source']) + ',\n' +
    '  "licence":' + JSON.stringify(puzzle['licence']) + ',\n' +
    '  "notice":' + JSON.stringify(puzzle['notice']) + ',\n' +
    '  "difficulty":' + JSON.stringify(puzzle.difficulty) + ',\n' +
    '  "grid":[\n' +
    puzzle.grid.map(function (line) {
      return '    ["' + line.join('","') + '"]'
    }).join(',\n') +
    '\n]}';
}

function download(filename, contents) {
  var element = $('<a/>', {
    href: 'data:text/plain;charset=utf-8,' + encodeURIComponent(contents),
    download: filename
  }).css({
    display: 'none'
  })
  element.appendTo($('body'))
  element.get()[0].click()
  element.remove()
}

function downloadPuzzle(puzzle) {
  var contents = exportPuzzle(puzzle)
  download(puzzle.title + '.json', contents)
}
