var settings = {}
var puzzleHash = 0
var game = null
var selectedColor = 0
var colors = [] // colors value orderd by hue
var activeBox = null
var pauseBox = null
var progressions = null
var actionHistory = new History()
var autopause = false
var lastcheatnotfound = false

var output = console.log.bind(window.console) // Used by bitmap.js


// --- Boxes ---

function show(element) {
  element.css('display', '')
  element.css('opacity', '1.0')
  element.attr('data-show', '1')
}

function hide(element, remove) {
  element.css('opacity', 0.0)
  element.removeAttr('data-show')
  setTimeout(function () {
    if (element.attr('data-show') === '1')
      return
    if (remove)
      element.remove()
    else
      element.css('display', 'none')
    }, 500);
}

function splashText(text, timeout) {
  var text = $('<div class="splash"></div>').css('opacity', 0.0).text(text)
  text.appendTo('body')
  show(text)
  if (timeout)
    setTimeout(hide, timeout*1000, text, true)
  return text
}

function toggleBox(box, display) {
  var active = box.is(activeBox)
  if (active && display !== true ||
      !active && activeBox !== null && display !== false) {
    hide(activeBox)
    activeBox = null
    // Deactivate handler (see below)
    $(window).off("click")
  }
  if (!active && display !== false) {
    activeBox = box
    show(activeBox)
  }
}

function togglePuzzlesBox(display) {
  updatePuzzles(progressions)
  toggleBox($('#puzzles'), display)
}

function toggleInfoBox(display) {
  toggleBox($('#info'), display)
}

function toggleHelpBox(display) {
  toggleBox($('#help'), display)
}

function toggleSettingsBox(display) {
  updateSettings()
  toggleBox($('#settings'), display)
}


// --- Progression storage ---

function saveProgress() {
  save('progressions', progressions);
}

function changeProgress() {
  progressions[puzzleHash] = {
      progress: game.getHintsProgress(),
      time: game.getElapsedTime()
    }
  saveProgress()
}

function resetProgress() {
  delete progressions[puzzleHash];
  saveProgress()
}

function resetAllProgress() {
  for (var puzzleHash in progressions)
    save('puzzle' + puzzleHash, '')
  progressions = {}
  saveProgress()
}


// --- Game Storage ---

function hash(s) {
  var h = 0
  for (var i = 0; i < s.length; i++)
    h = (h<<5)-h+s.charCodeAt(i)
  return h
}

function load(key) {
  try {
    var item = localStorage.getItem(key)
    return JSON.parse(item)
  }
  catch (e) {
    console.error(e)
    return null
  }
}

function save(key, value) {
  try {
    var item = JSON.stringify(value)
    localStorage.setItem(key, item)
  }
  catch (e) {
    console.error(e)
  }
}

function saveGame() {
  save ('puzzle' + puzzleHash, game.save())
}

function loadGame(data) {
  closeGame()

  puzzleHash = hash(JSON.stringify(data.grid))
  var gameState = load('puzzle' + puzzleHash)

  // Compatibility of saved state
  if (gameState && gameState.gameState) {
    game = new Game(data, gameState.gameState)
    game.startDate = gameState.timestamp
  }
  else {
    game = new Game(data, gameState)
  }
  colors = Object.keys(game.colors).sort(sortColor)
  updateCheatCount()
}

function resetGame() {
  save('puzzle' + puzzleHash, '')
  game = null
}

function closeGame() {
  // When closing the game, stop the timer so it doesn't count until the next
  // opening.
  if (game) {
    game.pause()
    changeProgress()
    saveGame()
  }
}


// --- State management ---

function saveState(push) {
  try {
    var state = {
      selectedColor: selectedColor,
      data: game.data
    }
    if (push)
      history.pushState(state, '', '')
    else
      history.replaceState(state, '', '')
  }
  catch (e) {
    // This often happens when the state is too big
    console.error(e)
  }
}

function loadState(state) {
  loadGame(state.data)
  selectedColor = state.selectedColor
  start()
}


// --- Main actions ---

function pause(pause) {
  if (game.isFinished())
    pause = false;

  if (pause === true)
    game.pause()
  else if (pause === false)
    game.resume()
  else
    game.togglePause()

  if (game.isPaused()) {
    if (!pauseBox)
      pauseBox = splashText('Paused')
    else
      show(pauseBox)
    hide($('#game'))
    $('#button-pause').attr('data-icon', '\uf01d')

  }
  else {
    if (pauseBox)
      hide(pauseBox)
    show($('#game'))
    $('#button-pause').attr('data-icon', '\uf28c')
  }
  saveGame()
}

function start() {
  togglePuzzlesBox(false)
  pause(false)
  actionHistory.reset()
  updateMenu()
  displayAll()
}

function changePuzzle(data) {
  loadGame(data)
  selectedColor = colors[0]
  start()
  saveState(true)
}

function selectPuzzle(url) {
  $.get(url, changePuzzle, 'json')
}

function resetPuzzle() {
  var puzzle = game.data
  resetGame()
  resetProgress()
  changePuzzle(puzzle)
}


// --- Startup / Cleanup / Focus ---

$(window).on('popstate', function(e) {
  loadState(e.originalEvent.state)
  displayAll()
})

$(document).ready(function() {
  document.oncontextmenu = function() {
    return false
  }

  settings = load('settings')
  if (!settings)
    settings = {}
  if (!("firstVisit" in settings)) {
    toggleHelpBox()
    settings.firstVisit = false
  }
  updateSettings()

  progressions = load('progressions')
  if (!progressions)
    progressions = {}

  if (history.state)
    loadState(history.state)
  else
    selectPuzzle('puzzles/wine.json')

  loadPuzzlesList()

  setInterval(updateTime, 50)
})

$(window).unload(function () {
  closeGame()
})

$(window).blur(function (e) {
  autopause = game && !game.isPaused()
  if (autopause)
    pause(true)
})

$(window).focus(function (e) {
  if (autopause)
    pause(false)
})

$(document).click(function(event) {
  if(!$(event.target).closest('#header').length) {
    if (activeBox && !$(event.target).closest('.box').length)
      toggleBox(activeBox, false);
    if (game.isPaused())
      pause(false)
  }
})


// --- Solver ---

var solve_timer = null
function toggleSolver() {
  if (!solve_timer) {
    var s = solve(game)
    solve_timer = setInterval(function() {
      var p = s.next()
      changeProgress()
      updateTiles()
      updateCanvas()
      updateHints()
      updateColors()
      updateProgress()
      if (p.done)
        toggleSolver()
    }, 0)
  }
  else {
    clearInterval(solve_timer)
    solve_timer = null
  }
}


// --- Puzzle selection ---

function loadPuzzlesList() {
  function toList(puzzles) {
    var list = []
    for (var p in puzzles) {
      var node
      if ('title' in puzzles[p])
        node = puzzles[p]
      else
        node = {puzzles: toList(puzzles[p])}
      node.path = p
      list.push(node)
    }
    list.sort(function (n1, n2) {
        if ('puzzles' in n1 == 'puzzles' in n2)
          return n1.path.localeCompare(n2.path)
        else if ('puzzles' in n1)
          return -1
        else
          return 1
      })
    return list
  }

  $.get('list-puzzles.php', function(data) {
    try {
      var puzzles = JSON.parse(data)
      displayPuzzles(toList(puzzles))
    }
    catch (e) {
      loadPuzzlesIndex()
      throw e
    }
  }, 'text')
}

function loadPuzzlesIndex() {
  $.get('puzzles/', function(data) {
    var puzzles = []
    var re = /<a[^>]*>([^<]+)\.json<\/a>/ig
    var match
    while (match = re.exec(data)) {
      puzzles.push({
        path: 'puzzles/' + match[1] + '.json',
        title: match[1]})
    }
    displayPuzzles(puzzles)
  })
}

$('#localpuzzle').change(function(e) {
  loadPuzzle(this.files[0], changePuzzle)
})

/* Puzzle change click */
$('#puzzles').on('click', '.puzzlink', function() {
  var url = $(this).attr('data-url')
  selectPuzzle(url)
})

$('#puzzles').on('click', '#reset-progress', function() {
  if ($(this).hasClass("confirm")) {
    resetAllProgress()
    resetPuzzle()
    updatePuzzles(progressions)
    $(this).removeClass("confirm")
  }
  else {
    $(this).addClass("confirm")
  }
})


// --- Color selection ---

function selectColor(color) {
  if (color != selectedColor) {
    selectedColor = color
    saveState()
    generateHints()
    updateTiles()
    updateColors()
  }
}

function previousColor() {
  var start = colors.indexOf(selectedColor)
  var n = colors.length;

  for (var i = (start + n - 1) % n; i != start; i = (i + n - 1) % n) {
    var c = colors[i]
    if (game.colors[c].valid != 'valid') {
      selectColor(c)
      return
    }
  }
}

function nextColor() {
  var start = colors.indexOf(selectedColor)
  var n = colors.length;

  for (var i = (start + 1) % n; i != start; i = (i + 1) % n) {
    var c = colors[i]
    if (game.colors[c].valid != 'valid') {
      selectColor(c)
      return
    }
  }
}

$('#colors').on("click", ".color", function() {
  selectColor($(this).attr('data-color'))
})

$('#colors').on("click", "#previous-color", function() {
  previousColor()
})

$('#colors').on("click", "#next-color", function() {
  nextColor()
})



// --- History ---

function undo() {
  if (!game.isFinished()) {
    actionHistory.undo()
    updateMenu()
    updateColors()
  }
}

function redo() {
  if (!game.isFinished()) {
    actionHistory.redo()
    updateMenu()
    updateColors()
  }
}


// --- Puzzle actions ---

function ActionSet(y,x,color) {
  var previous  = game.get(y,x)

  this.execute = function() {
    if (color)
      selectColor(color)
    else if (previous)
      selectColor(previous)
    game.set(y,x,color)
    updateAt(x,y)
    lastcheatnotfound = false
  }

  this.restore = function() {
    if (color)
      selectColor(color)
    else if (previous)
      selectColor(previous)
    game.set(y,x,previous)
    updateAt(x,y)
    lastcheatnotfound = false
  }
}

function ActionCross(y,x,color,state) {
  this.execute = function() {
    selectColor(color)
    game.setCross(y,x,color,state)
    updateAt(x,y)
  }

  this.restore = function() {
    selectColor(color)
    game.setCross(y,x,color,!state)
    updateAt(x,y)
  }
}


function updateAt(x, y) {
  updateTileAt(x, y)
  updateHintsAt(x, y)
  updateCanvasAt(x, y)
}

function finalizeEvent(e) {
  changeProgress()
  saveGame()
  updateColors()
  updateProgress()
  updateMenu()
  e.preventDefault()
}


function changeTile(x, y, mode, range) {
  if (game.isFinished())
    return;

  if (range !== undefined) {
    if (range === 'line' && x === undefined && y !== undefined) {
      for (var x = 0; x < game.height; x++)
        changeTile(x, y, mode)
    }
    else if (range === 'line' && x !== undefined && y === undefined ){
     for (var y = 0; y < game.width; y++)
        changeTile(x, y, mode)
    }
    else if (range === 'tile' && x !== undefined && y !== undefined) {
      changeTile(x, y, mode)
    }
    return
  }

  switch (mode) {
    case 'none':
      return

    case 'fill':
      if (game.isEmpty(y,x,selectedColor))
        actionHistory.add(new ActionSet(y,x,selectedColor))
      break

    case 'erase':
      if (game.get(y,x) === selectedColor)
        actionHistory.add(new ActionSet(y,x,false))
      break

    case 'cross':
      if (game.isEmpty(y,x,selectedColor))
        actionHistory.add(new ActionCross(y,x,selectedColor,true))
      break

    case 'empty':
      if (game.get(y,x) === selectedColor)
        actionHistory.add(new ActionSet(y,x,false))
      // Continue to the next case !
    case 'uncross':
      /* Because the user is dump and WILL try to uncross blank columns */
      if (game.isFillable(y,x,selectedColor))
        actionHistory.add(new ActionCross(y,x,selectedColor,false))
      break
  }
  if (mode == 'fill' && game.isFinished()) {
    confetti.start(6000.0)
    splashText('You win !', 4.0)
  }

}


function selectMode(button, x, y) {
  // If the tile is filled or crossed, the mode is to remove what's in
  // the tile, regardless of the button pressed
  if (x !== undefined && y !== undefined) {
    if (game.isCrossed(y,x,selectedColor))
      return 'uncross'
    else if (game.get(y,x) === selectedColor)
      return 'erase'
  }
  // Otherwise, we choose the mode depending on the button
  switch (button) {
  case 0: return 'fill'
  case 1: return 'empty'
  case 2: return 'cross'
  default: return 'none' // do nothing
  }
}

var mode = 'none'
var range = 'tile'
/* Tile click handler */
$('#grid').on('mousedown', 'td', function(e) {
  var x = $(this).attr('data-x')
  var y = $(this).attr('data-y')
  mode = selectMode(e.button, x, y) // change global mode
  range = x === undefined || y === undefined ? 'line' : 'tile'
  changeTile(x, y, mode, range)
  finalizeEvent(e)
  updateCursor($(this))
})

var x = 0, y = 0
$('#grid').on('mousemove', 'td', function(e) {
  var _x = $(this).attr('data-x')
  var _y = $(this).attr('data-y')
  if (_x != x || _y != y) {
    x = _x
    y = _y
    if (e.buttons != 0) {
      changeTile(x, y, mode, range)
      finalizeEvent(e)
    }
    updateCursor($(this))
  }
})

$(document).on('mouseup', function(e) {
  actionHistory.end_chain()
})

$('#grid').mouseleave(function(e) {
  hideCursor();
})


// --- Cheat ---

function cheat(e) {
  if (game.isFinished() || lastcheatnotfound)
    return;

  var coord = game.removeError()
  if (coord) {
    updateAt(coord.y, coord.x)
    displayTileAsErroneous(coord.y, coord.x)
  }
  else {
    lastcheatnotfound = true
    splashText("There are no errors !", 1.0)
  }
  updateCheatCount()
  finalizeEvent(e)
}


// --- Menu ---

function disable($element) {
  $element.addClass("disabled")
}

function enable($element) {
  $element.removeClass("disabled")
}

function setEnabled($element, enabled) {
  if (enabled)
    enable($element)
  else
    disable($element)
}

function updateMenu() {
  setEnabled($('#button-pause'), !game.isFinished())
  setEnabled($('#button-cheat'), !game.isFinished() && !lastcheatnotfound)
  setEnabled($('#button-undo'), !game.isFinished() && actionHistory.canUndo())
  setEnabled($('#button-redo'), !game.isFinished() && actionHistory.canRedo())
}

$('#button-pause').click(pause)
$('#button-info').click(toggleInfoBox)
$('#button-change').click(togglePuzzlesBox)
$('#button-undo').click(undo)
$('#button-redo').click(redo)
$('#button-reset').click(resetPuzzle)
$('#button-help').click(toggleHelpBox)
$('#button-settings').click(toggleSettingsBox)
$('#button-cheat').click(cheat)

$('#download-button').click(function(e) {
  downloadPuzzle(game.data)
})


// --- Settings ---

function updateSettings() {
  var css

  switch (settings.menu) {
    default: settings.menu = 'left'
    case 'left': css = 'styles/leftmenu.css'; break
    case 'top': css = 'styles/topmenu.css'; break
  }
  $('#menu-style-sheet').attr('href', css)

  switch (settings.background) {
    default: settings.background = 'bright'
    case 'bright': css = 'styles/bright.css'; break
    case 'dark': css = 'styles/dark.css'; break
  }
  $('#background-style-sheet').attr('href', css)

  $('#header').removeClass()
  $('#header').addClass(settings.menu)
  $('#header').addClass(settings.background)
  save('settings', settings)
}

function switchMenuLayout() {
  settings.menu = settings.menu != 'left' ? 'left' : 'top'
  updateSettings()
}

function switchBackground() {
  settings.background = settings.background != 'bright' ? 'bright' : 'dark'
  updateSettings()
}

$('#button-menu-layout').click(switchMenuLayout)
$('#button-background').click(switchBackground)
$('#button-solve').click(toggleSolver)


// --- Scrolling ---

var scroll = {
  xtarget : 0,
  ytarget : 0,
  xspeed : 0,
  yspeed : 0,
  maxspeed : 500,
  acceleration : 1000,
  update : null,
  lastevent : null
}

function updateScroll() {
  const acc = scroll.acceleration

  let now = new Date().getTime()
  let delta = (now - scroll.lastevent) * 0.001
  scroll.lastevent = now

  if (scroll.xspeed < scroll.xtarget)
    scroll.xspeed = Math.min(scroll.xtarget, scroll.xspeed + acc * delta)
  else if (scroll.xspeed > scroll.xtarget)
    scroll.xspeed = Math.max(scroll.xtarget, scroll.xspeed - acc * delta)

  if (scroll.yspeed < scroll.ytarget)
    scroll.yspeed = Math.min(scroll.ytarget, scroll.yspeed + acc * delta)
  else if (scroll.yspeed > scroll.ytarget)
    scroll.yspeed = Math.max(scroll.ytarget, scroll.yspeed - acc * delta)

  if (Math.abs(scroll.xspeed - scroll.xtarget) < 0.01 &&
    Math.abs(scroll.yspeed - scroll.ytarget) < 0.01 &&
    Math.abs(scroll.xspeed) < 0.01 &&
    Math.abs(scroll.yspeed) < 0.01) {
    clearInterval(scroll.update)
    scroll.update = null;
  }
  else {
    $('#tiles')[0].scrollBy(scroll.xspeed * delta, scroll.yspeed * delta)
  }
}

function scrollStart() {
  if (!scroll.update) {
    scroll.update = setInterval(updateScroll, 20)
    scroll.lastevent = new Date().getTime()
  }
}

function scrollHorizontally(direction, active) {
  if (active)
    scroll.xtarget = direction * scroll.maxspeed
  else
    scroll.xtarget = 0

  scrollStart()
}

function scrollVertically(direction, active) {
  if (active)
    scroll.ytarget = direction * scroll.maxspeed
  else
    scroll.ytarget = 0

  scrollStart()
}

function scrollEvent(event, down) {
  if (event.originalEvent.repeat)
    return

  switch (event.originalEvent.code) {
  case "ArrowRight" :
  case "KeyD" :
    scrollHorizontally(1, down)
    break

  case "ArrowLeft" :
  case "KeyA" :
    scrollHorizontally(-1, down)
    break

  case "ArrowUp" :
  case "KeyW" :
    scrollVertically(-1, down)
    break

  case "ArrowDown" :
  case "KeyS" :
    scrollVertically(1, down)
    break

  default:
    return
  }

  event.preventDefault()
}

// --- Keyboard shortcuts ---

$(document).keyup(function(event) {
  scrollEvent(event, false);
})

$(document).keydown(function(event) {
  scrollEvent(event, true);

  // Each case handled in this switch does break without return, thus
  // triggering the preventDefault call
  switch (String.fromCharCode(event.which)) {
  case "\t" :
    if (event.shiftKey)
      previousColor()
    else
      nextColor()
    break

  case "P" :
    pause()
    break

  case "Z" :
    if (event.ctrlKey) {
      if (event.shiftKey)
        redo()
      else
        undo()
    }
    else {
      return
    }
    break

  default :
    return
  }

  event.preventDefault()
})
