var similarityDistance = 16.0
var maxSide = 320
var maxArea = 65536

function loadBitmap(file, onload) {
  output("Loading file", file.name, "...")
  var reader = new FileReader()
  reader.addEventListener("load", function () {
      try {
        var bitmap = readBitmap(this.result)
        reduceColors(bitmap)
        clampBitmap(bitmap)
        onload(bitmap)
      }
      catch (error) {
        if ('bitmap_error' in error)
          output("Failed to load bitmap: ", error.bitmap_error)
        else
          throw error
      }
    }, false)
  reader.readAsArrayBuffer(file)
}

function readBitmap(buffer) {
  // Extract header from file
  var data = new DataView(buffer)
  var magic_number  = data.getUint16(0x00, true)
  var bitmap_start  = data.getUint32(0x0A, true)
  var header_size   = data.getUint32(0x0E, true)
  var width         = data.getInt32(0x12, true)
  var height        = data.getInt32(0x16, true)
  var bpp           = data.getInt16(0x1C, true)
  var compression   = data.getInt16(0x1E, true)

  // Interpret header
  if (magic_number != 0x4D42 || header_size < 40 || compression != 0)
    throw {'bitmap_error':"unsupported format."}

  var area = width * height
  if (width > maxSide || height > maxSide ||
      width < 1   || height < 1 || area > maxArea)
    throw {'bitmap_error':"image too large."}

  // Exctract data
  var data = new Uint8Array(buffer, bitmap_start)

  // Convert data to bitmap
  var row_size = Math.ceil(3 * width / 4) * 4
  var bitmap = new Array(height)
  for (var y = 0 ; y < height ; y++ ) {
    bitmap[y] = new Array(width)
    for (var x = 0 ; x < width ; x++) {
      var offset = x * 3 + row_size * (height - 1 - y)
      var c = rgb(data[offset+2], data[offset+1], data[offset])
      bitmap[y][x] = rgbToColor(c)
    }
  }

  return bitmap
}

function countColors(bitmap) {
  var colors = {}
  for (var y = 0; y < bitmap.length ; y++) {
    for (var x = 0; x < bitmap[y].length; x++) {
      var c = bitmap[y][x];
      colors[c] = c in colors ? colors[c] + 1 : 1
    }
  }
  return colors
}

function mergeColors(bitmap, c1, c2) {
  output("Merging color", c2, "into dominant color", c1)
  for (var y = 0; y < bitmap.length ; y++) {
    for (var x = 0; x < bitmap[y].length; x++) {
      if (bitmap[y][x] == c2)
        bitmap[y][x] = c1
    }
  }
}

function reduceColors(bitmap) {
  var colors = countColors(bitmap)
  do {
    var changes = false
    for (var c1 in colors) {
      var nearest = c1
      var distance = similarityDistance
      for (var c2 in colors) {
        d = rgbDistance(colorToRgb(c1), colorToRgb(c2))
        if (c1 != c2 && d < distance) {
          nearest = c2
          distance = d
        }
      }
      if (nearest != c1) {
        c2 = nearest
        if (colors[c1] > colors[c2]) {
          mergeColors(bitmap, c1, c2)
          delete colors[c2]
        }
        else {
          mergeColors(bitmap, c2, c1)
          delete colors[c1]
        }
        changes = true
        break;
      }
    }
  }
  while (changes)
}

function removeBitmapRow(bitmap, y) {
  bitmap.splice(y, 1);
}

function removeBitmapColumn(bitmap, x) {
  for (var y = 0; y < bitmap.length ; y++) {
    bitmap[y].splice(x, 1);
  }
}

function isBlankRow(bitmap, y) {
  let color = bitmap[y][0];
  for (var x = 1; x < bitmap[y].length; x++) {
    if (bitmap[y][x] != color)
      return false;
  }
  return true;
}

function isBlankColumn(bitmap, x) {
  let color = bitmap[0][x];
  for (var y = 1; y < bitmap.length; y++) {
    if (bitmap[y][x] != color)
      return false;
  }
  return true;
}

function clampBitmap(bitmap) {
  // Remove first rows
  while (isBlankRow(bitmap, 0)) {
    removeBitmapRow(bitmap, 0);
  }
  // Remove last row
  while (isBlankRow(bitmap, bitmap.length-1)) {
    removeBitmapRow(bitmap, bitmap.length-1);
  }
  // Remove first column
  while (isBlankColumn(bitmap, 0)) {
    removeBitmapColumn(bitmap, 0);
  }
  // Remove last lines
  while (isBlankColumn(bitmap, bitmap[0].length-1)) {
    removeBitmapColumn(bitmap, bitmap[0].length-1);
  }
}
