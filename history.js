
function History()
{

  this.end_chain = function() {
    this.action_complete = true 
  }

  this.add = function(action) {
    if (this.action_complete) {
      this.action_complete = false
      this.current++
      this.actions.splice(this.current)
      this.actions[this.current] = [action]
    }
    else {
      this.actions[this.current].push(action)
    }
    action.execute()
  }

  this.canUndo = function() {
    return this.current >= 0
  }

  this.canRedo = function() {
    return this.current < this.actions.length - 1
  }

  this.undo = function() {
    if (this.current >= 0) {
      for (let action of this.actions[this.current])
        action.restore()
      this.current--
    }
  }

  this.redo = function() {
    if (this.current < this.actions.length - 1) {
      this.current++
      for (let action of this.actions[this.current])
        action.execute()
    }
  }

  this.reset = function() {
    this.actions = []
    this.current = -1
    this.action_complete = true // If last action have been completed
  }

  this.reset()
}

