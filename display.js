function formatTime(t) {
  var days = Math.floor(t / (24*60*60*1000))
  var hours = Math.floor(t / (60*60*1000)) % 24
  var minutes = Math.floor(t / (60*1000)) % 60
  var seconds = Math.floor(t / 1000) % 60
  return '' +
    (days > 0 ? days + 'd ' : '') +
    (hours > 0 ? hours + 'h ' : '') +
    (minutes > 0 ? minutes + 'm ' : '') +
    seconds + 's'
}

function drawProgress(progress)
{
  var v = Math.floor(progress * 100)
  var color =
    progress >= 1.0 ? '#00A000' :
    '#' + rgbToColor(rgbLerp(
      rgb(0xA0,0xA0,0x00),
      rgb(0xA0,0x00,0x00),
      progress))
  var write =
    '<div class="progressbar">' +
      '<div style="width:' + v + '%; background-color: ' + color + ';">' +
      '</div>' +
    '</div>' +
    '<span style="color: ' + color + ';">' + v + '%</span>'
  return write
}


/* --- Initial display --- */

function displayPuzzles(puzzles) {
  var write =
    '<p>Select a puzzle from the server:</p>' +
    '<div id="puzzle-list">' +
      '<table>' +
        '<tr><th>Title</th><th>Size</th><th>Difficulty</th>' +
        '<th colspan="2">Progress</th></tr>' +
        displayPuzzlesDir(puzzles, 0) +
      '</table>' +
    '</div>'
  $('#puzzles').prepend(write)
}

function displayPuzzlesDir(puzzles, depth) {
  write = '';
  for (var i = 0; i < puzzles.length; i++) {
    var node = puzzles[i];
    if ('puzzles' in node) {
      write +=
        '<tr class="puzzdir">' +
          '<td colspan="5">' + node.path + '</td>' +
        '</tr>' +
        displayPuzzlesDir(node.puzzles, depth+1)
    }
    else {
      write +=
        '<tr ' +
            'data-url="' + node.path + '" ' +
            'data-hash="' + node.hash + '" ' +
            'class="puzzlink">' +
          '<td style="padding-left: ' + (4 + 8*depth) + 'px;">' +
           node.title + '</td>' +
          '<td>' + node.width + 'x' + node.height + '</td>' +
          '<td>' + node.difficulty + '</td>' +
          '<td></td>' +
          '<td></td>' +
        '</tr>'
    }
  }
  return write;
}

function displayColors() {
  /* Displaying the colors */
  var writing = '<div class="color-navig" id="previous-color">&#x25B2;</div>'
  for (var i = 0; i < colors.length; i++) {
    var color = colors[i]
    writing += '<div class="color" data-color="' + color + '"  id="color-' + color + '" style="background-color:#' + color + '"></div>'
  }
  writing += '<div class="color-navig" id="next-color">&#x25BC;</div>'
  $('#colors').html(writing)
  updateColors();
}

function generateHints() {
  for (var i = 0; i < game.height; i++) {
    var writing = ''
    game.getRowHints(i,selectedColor).forEach(function (hint) {
      var classes = hint.number >= 100 ? 'hint long' : 'hint'
      writing +=
        '<span class="' + classes + '" id="hint' + hint.no + '">' +
        hint.number + '</span>'
    })
    $('#rowHint' + i).html(writing)
  }

  for (var j = 0; j < game.width; j++) {
    var writing = ''
    game.getColHints(j,selectedColor).forEach(function (hint) {
      var classes = hint.number >= 100 ? 'hint long' : 'hint'
      writing +=
        '<span class="' + classes + '" id="hint' + hint.no + '">' +
        hint.number + '</span>'
    })
    $('#colHint' + j).html(writing)
  }

  updateHints()
}

function displayGrid() {
  canvasSize = initCanvas()
  rowHintsWidth = Math.max(canvasSize.width, game.rowHints.maxHints * 20)
  colHintsHeight = Math.max(canvasSize.height, game.colHints.maxHints * 20)

  var writing = ''

  writing += '<div class="layout" style="flex-basis: ' + colHintsHeight + 'px;">'

  // Canvas
  writing += '<div style="flex-basis: ' + rowHintsWidth + 'px;">'
  writing += '<canvas id="canvas" width="' + canvasSize.width + '" height="' + canvasSize.height + '"></canvas>'
  writing += '</div>'

  // Col hints
  writing += '<div id="colHints"><table style="height: ' + colHintsHeight + 'px;" ><tr>'
  for (var j = 0; j < game.width; j++)
    writing += '<td id="colHint' + j + '" data-y="' + j + '"></td>'
  writing += '<td class="blank"></td>'
  writing += '</tr></table></div>'

  writing += '</div>'
  writing += '<div class="layout">'

  // Row hints
  writing += '<div id="rowHints" style="flex-basis: ' + rowHintsWidth + 'px;">'
  writing += '<table style="width: ' + rowHintsWidth + 'px;">'
  for (var i = 0; i < game.height; i++)
    writing += '<tr><td id="rowHint' + i + '" data-x="' + i + '"></td></tr>'
  writing += '<tr class="blank"><td></td></tr>'
  writing += '</table></div>'

  // Tiles

  writing += '<div id="tiles">'
  writing += '<div id="blocks"></div>'
  writing += '<table>'
  for (var i = 0; i < game.height; i++) {
    writing += '<tr>'
    for (var j = 0; j < game.width; j++) {
      writing += '<td id="tile-' + i + '-' + j + '"'
      writing += ' data-x="' + i + '" data-y="' + j + '"></td>'
    }
    writing += '</tr>'
  }
  writing += '</table>'
  writing += '</div>'

  writing += '</div>'
  $('#grid').html(writing)

  /* Scrolling */
  $('#tiles').scroll(function(e) {
    $('#rowHints').scrollTop($("#tiles").scrollTop());
    $('#colHints').scrollLeft($("#tiles").scrollLeft());
    hideCursor();
  })
  /* Setup grid contents */
  generateHints()
  updateTiles()
  updateCanvas()
  /* Allocate blocks */
  rowBlocks = new BlockPool("rowblock")
  colBlocks = new BlockPool("colblock")
}

// Display all except puzzles
function displayAll() {
  displayColors()
  displayGrid()
  updateInfos()
  updateProgress()
}


/* --- Display update --- */

function updatePuzzles(progressions) {
  $('.puzzlink').each(function (index, element) {
    var hash = $(this).attr('data-hash')
    if (hash in progressions) {
      $(this).children("td:nth-child(4)")
        .html(drawProgress(progressions[hash].progress))
      $(this).children("td:nth-child(5)")
        .text(formatTime(progressions[hash].time))
    } else {
      $(this).children("td:nth-child(4)").empty()
      $(this).children("td:nth-child(5)").empty()
    }
  })
}

function updateColor(c) {
  var element = $('#color-' + c);
  element.removeClass('validColor invalidColor selectedColor');
  if (game.colors[c].valid == 'valid')
    element.addClass('validColor')
  else if (game.colors[c].valid == 'invalid')
    element.addClass('invalidColor')
}

function updateColors() {
  for (var c in game.colors)
    updateColor(c)
  $('#color-' + selectedColor).addClass('selectedColor')
}

function updateHint(hint) {
  var element = $('#hint' + hint.no)
  element.removeClass('validHint invalidHint')
  if (hint.state == 'valid')
    element.addClass('validHint')
  else if (hint.state == 'invalid')
    element.addClass('invalidHint')
}

function updateHintsAt(x, y) {
  game.getRowHints(x,selectedColor).forEach(updateHint)
  game.getColHints(y,selectedColor).forEach(updateHint)
}

function updateHints() {
  for (var i = 0; i < game.height; i++)
    game.getRowHints(i,selectedColor).forEach(updateHint)
  for (var j = 0; j < game.width; j++)
    game.getColHints(j,selectedColor).forEach(updateHint)
}

function updateTileAt(x, y) {
  var element = $('#tile-' + x + '-' + y)
  /* Reset tile */
  element.css('background-color', '')
  element.css('border-color', '')
  element.removeClass('crossed highlight erroneous')
  /* Set contents */
  if (game.isFilled(y,x)) {
    color = game.get(y,x)
    element.css('background-color', '#' + color)
    element.css('border-color', '#' + color)
    if (color == selectedColor)
      element.addClass('highlight')
  }
  else if (game.isCrossed(y,x,selectedColor)) {
    element.css('color', '#' + selectedColor)
    element.addClass('crossed')
  }
}

function displayTileAsErroneous(x,y) {
  var element = $('#tile-' + x + '-' + y)
  element.addClass('erroneous')
  element[0].scrollIntoView({
      behavior: "smooth",
      block: "center",
      inline: "center"
  })
}

function updateTiles() {
  /* Updating tiles */
  for (var i = 0; i < game.height; i++) {
    for (var j = 0; j < game.width; j++) {
      updateTileAt(i, j);
    }
  }
}


function BlockPool(className)
{
  var pool = []
  var index = 0;

  this.next = function () {
    if (!pool[index])
      pool[index] = $('<div><span></div>').addClass(className).appendTo($('#blocks'))
    return pool[index++].show()
  }

  this.end = function () {
    while (index < pool.length)
      pool[index++].hide()
    index = 0;
  }

}

var rowBlocks, colBlocks;

function hideCursor() {
  $('#rowcursor,#colcursor').remove()
  rowBlocks.end()
  colBlocks.end()
}

function updateCursor(cell) {
  var $grid = $('#grid')
  var table = cell.closest('table')

  // Create the cursor if necessary
  var $rowcursor = $('#rowcursor').length ? $('#rowcursor') :
    $('<div id="rowcursor"></div>').hide().appendTo($('#grid'))
  var $colcursor = $('#colcursor').length ? $('#colcursor') :
    $('<div id="colcursor"></div>').hide().appendTo($('#grid'))

  // Remove previous matched hints
  $('.matchedHint').removeClass('matchedHint')

  // Extract cursor coordinates
  var x = cell.attr('data-x'), y = cell.attr('data-y')

  if (x) {
    $rowcursor.show().css({
      position: 'absolute',
      left: $grid.position().left,
      top: cell.position().top - 1,
      width: $grid.width(),
      height: cell.height()
    })

    var blocks = game.getRowBlocks(x,selectedColor)
    var hints = game.getRowHints(x,selectedColor)
    if (y && game.getState(y,x,selectedColor) == 'filled') {
      var matched_hints = findMatchedHints(hints, blocks, y)
      if (matched_hints !== false) {
        for (var i = matched_hints.l ; i <= matched_hints.u ; i++) {
          $('#hint' + hints[i].no).addClass('matchedHint')
        }
      }
    }
    blocks = blocks.filter(isFilledBlock)
    for (var i = 0; i < blocks.length; i++) {
      var block = blocks[i]
      var start = $('#tile-' + x + '-' + block.start)
      var end = $('#tile-' + x + '-' + block.end)
      rowBlocks.next().css({
        position: 'absolute',
        left: start.position().left,
        top: cell.position().top,
        width: end.position().left + end.width() - start.position().left,
        height: cell.height()
      }).children('span').text(block.size)
    }
  }
  else {
    $rowcursor.hide()
  }

  if (y) {
    $colcursor.show().css({
      position: 'absolute',
      left: cell.position().left - 1,
      top: $grid.position().top,
      width: cell.width(),
      height: $grid.height()
    })

    var blocks = game.getColBlocks(y,selectedColor)
    var hints = game.getColHints(y,selectedColor)
    if (x && game.getState(y,x,selectedColor) == 'filled') {
      var matched_hints = findMatchedHints(hints, blocks, x)
      if (matched_hints !== false) {
        for (var i = matched_hints.l ; i <= matched_hints.u ; i++) {
          $('#hint' + hints[i].no).addClass('matchedHint')
        }
      }
    }
    blocks = blocks.filter(isFilledBlock)
    for (var i = 0; i < blocks.length; i++) {
      var block = blocks[i]
      var start = $('#tile-' + block.start + '-' + y)
      var end = $('#tile-' + block.end + '-' + y)
      colBlocks.next().css({
        position: 'absolute',
        left: cell.position().left,
        top: start.position().top,
        width: cell.width(),
        height: end.position().top + end.height() - start.position().top
      }).children('span').text(block.size)
    }
  }
  else {
    $colcursor.hide()
  }

  rowBlocks.end()
  colBlocks.end()
}

function updateTime() {
  if (game) {
    var t = game.getElapsedTime()
    $('#chrono').text(formatTime(t))
  }
}

function updateProgress() {
  var progress = game.getHintsProgress()
  $('#progress').html(drawProgress(progress))
}

function updateInfos() {
  function truncate(s, n) {
    return (s.length > n) ? s.substr(0,n-1)+'&hellip;' : s;
  };

  function get(field) {
    if (game.data[field])
      return game.data[field]
    else
      return '?'
  }

  $('#puzzle-title, #puzzle-difficulty').empty()
  $('#info-title, #info-original-title, #info-author, #info-source').empty()
  $('#info-difficulty, #info-notice, #info-licence').empty()

  if (game) {
    $('#puzzle-title').text(get('title'))
    $('#puzzle-difficulty').text(get('difficulty'))
    $('#info-title').text(get('title'))
    drawImage($('#info-image'))
    $('#info-attribution').empty().append(attribution(game.data))
    $('#info-size').text(game.width + 'x' + game.height)
    $('#info-difficulty').text(get('difficulty'))
  }
}

function updateCheatCount() {
  var displayed = game.cheatCount
  var suffix = ''
  if (game.cheatCount > 6) {
    displayed = 6
    suffix = '... (' + game.cheatCount + ')'
  }
  var text = Array(displayed+1).join('&#x2716;') + suffix
  $('#cheat-count').html(text)
}


/* --- Canvas --- */

var pixelSize

function drawPixel(ctx, S, y, x, color) {
  var rgb = colorToRgb(color)
  var lightcolor = rgbToColor(rgbLerp(rgb, white, 0.8))
  var darkcolor = rgbToColor(rgbLerp(rgb, black, 0.8))

  if (S > 2) {
    ctx.fillStyle = '#' + lightcolor
    ctx.fillRect(y * S, x * S, S, S)

    if (S > 3) {
      ctx.fillStyle = '#' + darkcolor
      ctx.fillRect(y * S + 1, x * S + 1, S - 1, S - 1)
      ctx.fillStyle = '#' + color
      ctx.fillRect(y * S + 1, x * S + 1, S - 2, S - 2)
    }
    else {
      ctx.fillStyle = '#' + color
      ctx.fillRect(y * S + 1, x * S + 1, S - 1, S - 1)
    }
  }
  else {
    ctx.fillStyle = '#' + color
    ctx.fillRect(y * S, x * S, S, S)
  }
}

function clearPixel(ctx, S, y, x) {
  ctx.clearRect(y * S, x * S, S, S)
}

function drawImage(canvas) {
  var S = 2
  canvas.attr('width', game.width * S)
  canvas.attr('height', game.height * S)
  var ctx = canvas.get(0).getContext("2d")
  ctx.width = game.width * S
  ctx.height = game.height * S

  for (var y = 0; y < game.height; y++)
    for (var x = 0; x < game.width; x++)
      drawPixel(ctx, S, x, y, game.data.grid[y][x])
}

function initCanvas() {
  pixelSize = Math.min(12, Math.floor(75 * Math.pow(Math.max(game.width, game.height), -0.7)))
  return {
    width: game.width * pixelSize,
    height: game.height * pixelSize
  }
}

function updateCanvas() {
  for (var y = 0; y < game.height; y++)
    for (var x = 0; x < game.width; x++)
      updateCanvasAt(y, x)
}

function updateCanvasAt(x, y) {
  var S = pixelSize
  var ctx = $('#canvas').get(0).getContext("2d")
  ctx.width = game.width * S
  ctx.height = game.height * S

  if (game.isFilled(y,x)) {
    var color = game.get(y,x)
    var rgb = colorToRgb(color)
    drawPixel(ctx, S, y, x, color)
  }
  else {
    clearPixel(ctx, S, y, x)
  }
}
