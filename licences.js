function cclicence(kind, version) {
  return {
      name: "CC " + kind.toUpperCase() + " " + version,
      url: 'https://creativecommons.org/licenses/' + kind + '/' + version,
      image: {
        src: 'https://licensebuttons.net/l/' + kind + '/' + version + '/88x31.png',
        width: 88, height: 31
      }
    }
}

var licences = {
  'gpl-2.0' :  {
      name: "GPL 2.0",
      url: "https://www.gnu.org/licenses/old-licenses/gpl-2.0.html",
      image: {
        src: 'https://licensebuttons.net/l/GPL/2.0/88x62.png',
        width: 88, height: 62
    }},
  'lgpl-2.1' :  {
      name: "LGPL 2.1",
      url: "https://www.gnu.org/licenses/old-licenses/lgpl-2.1.html",
      image: {
        src: 'https://licensebuttons.net/l/GPL/2.0/88x62.png',
        width: 88, height: 62
    }},
  'publicdomain' : {
      name: "Public Domain",
      image: {
        src: 'https://licensebuttons.net/l/publicdomain/88x31.png',
        width: 88, height: 31
    }},
  'cc-by-1.0' : cclicence("by", "1.0"),
  'cc-by-2.0' : cclicence("by", "2.0"),
  'cc-by-2.1' : cclicence("by", "2.1"),
  'cc-by-2.5' : cclicence("by", "2.5"),
  'cc-by-3.0' : cclicence("by", "3.0"),
  'cc-by-4.0' : cclicence("by", "4.0"),
  'cc-by-nc-1.0' : cclicence("by-nc", "1.0"),
  'cc-by-nc-2.0' : cclicence("by-nc", "2.0"),
  'cc-by-nc-2.1' : cclicence("by-nc", "2.1"),
  'cc-by-nc-2.5' : cclicence("by-nc", "2.5"),
  'cc-by-nc-3.0' : cclicence("by-nc", "3.0"),
  'cc-by-nc-4.0' : cclicence("by-nc", "4.0"),
  'cc-by-nc-nd-2.0' : cclicence("by-nc-nd", "2.0"),
  'cc-by-nc-nd-2.1' : cclicence("by-nc-nd", "2.1"),
  'cc-by-nc-nd-2.5' : cclicence("by-nc-nd", "2.5"),
  'cc-by-nc-nd-3.0' : cclicence("by-nc-nd", "3.0"),
  'cc-by-nc-nd-4.0' : cclicence("by-nc-nd", "4.0"),
  'cc-by-nc-sa-1.0' : cclicence("by-nc-sa", "1.0"),
  'cc-by-nc-sa-2.0' : cclicence("by-nc-sa", "2.0"),
  'cc-by-nc-sa-2.1' : cclicence("by-nc-sa", "2.1"),
  'cc-by-nc-sa-2.5' : cclicence("by-nc-sa", "2.5"),
  'cc-by-nc-sa-3.0' : cclicence("by-nc-sa", "3.0"),
  'cc-by-nc-sa-4.0' : cclicence("by-nc-sa", "4.0"),
  'cc-by-nd-1.0' : cclicence("by-nd", "1.0"),
  'cc-by-nd-2.0' : cclicence("by-nd", "2.0"),
  'cc-by-nd-2.1' : cclicence("by-nd", "2.1"),
  'cc-by-nd-2.5' : cclicence("by-nd", "2.5"),
  'cc-by-nd-3.0' : cclicence("by-nd", "3.0"),
  'cc-by-nd-4.0' : cclicence("by-nd", "4.0"),
  'cc-by-nd-nc-1.0' : cclicence("by-nd-nc", "1.0"),
  'cc-by-nd-nc-2.0' : cclicence("by-nd-nc", "2.0"),
  'cc-by-sa-1.0' : cclicence("by-sa", "1.0"),
  'cc-by-sa-2.0' : cclicence("by-sa", "2.0"),
  'cc-by-sa-2.1' : cclicence("by-sa", "2.1"),
  'cc-by-sa-2.5' : cclicence("by-sa", "2.5"),
  'cc-by-sa-3.0' : cclicence("by-sa", "3.0"),
  'cc-by-sa-4.0' : cclicence("by-sa", "4.0"),
  'cc-nc-1.0' : cclicence("nc", "1.0"),
  'cc-nc-2.0' : cclicence("nc", "2.0"),
  'cc-nc-sa-1.0' : cclicence("nc-sa", "1.0"),
  'cc-nc-sa-2.0' : cclicence("nc-sa", "2.0"),
  'cc-nd-1.0' : cclicence("nd", "1.0"),
  'cc-nd-2.0' : cclicence("nd", "2.0"),
  'cc-nd-nc-1.0' : cclicence("nd-nc", "1.0"),
  'cc-nd-nc-2.0' : cclicence("nd-nc", "2.0"),
  'cc-sa-1.0' : cclicence("sa", "1.0"),
  'cc-sa-2.0' : cclicence("sa", "2.0"),
  'cc-zero-1.0' : cclicence("zero", "1.0")
}

function licencebutton(licence) {
  return $('<img>', {...licence.image, alt:licence.name}).addClass('licence-button')
}

function attribution(data) {
  let output = $('<div class="attribution">')
  let licence = null
  if (data['licence'] && data['licence'] in licences) {
    licence = licences[data['licence']]
  }

  if (licence) {
    licencebutton(licence).appendTo(output)
  }

  let text = $('<div class="attribution-text">');

  let name; 
  if (data['original-title']) {
    name = data['original-title'];
  }
  else {
    name = data['title']
  }

  text.append('"')
  if (data['source']) {
    $('<a>')
      .text(name)
      .attr('href', data['source'])
      .appendTo(text)
  }
  else {
    text.append(document.createTextNode(name))
  }
  text.append('"')

  if (data['author']) {
    text.append(' by ')
    if (data['author-url']) {
      $('<a>')
        .text(data['author'])
        .attr('href', data['author-url'])
        .appendTo(text)
    }
    else {
      text.append(document.createTextNode(data['author']))
    }
  }

  if (licence) {
    text.append(', used under ')
    if (licence['url']) {
      $('<a>')
        .text(licence['name'])
        .attr('href', licence['url'])
        .appendTo(text)
    }
    else {
      text.append(licence['name'])
    }

    text.append()
  }

  if (data['notice']) {
    text.append(' / ')
    text.append(document.createTextNode(data['notice']))
  }

  output.append(text);
  return output;
}
