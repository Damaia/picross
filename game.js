/* The Game object stores everything about the game state, including
   - puzzle data,
   - computed hints and colors,
   - player found tiles and crosses,
   - timestamp of game start and end,
   - number of cheats used and
   - whether the player has won of note.
   It maintains the consistency of these data as long as the modification of
   the state uses the methods in this file.
*/

function Game(data, state) {
  this.setup(data)
  if (state)
    this.load(state)
  else
    this.reset()
}


// --- Getters and setters (Abstract the underlying datatypes) ---

Game.prototype.get = function (x,y) {
  return this.found[y][x]
}

Game.prototype.isCrossed = function (x,y,c) {
  return this.crosses[c].get(x,y)
}

Game.prototype.isFilled = function (x,y) {
  return this.found[y][x] !== false
}

Game.prototype.isEmpty = function (x,y,c) {
  return !this.isCrossed(x,y,c) && !this.isFilled(x,y)
}

Game.prototype.isFillable = function (x,y,c) {
  return this.colHints[c][x] && this.rowHints[c][y]
}

Game.prototype.isValid = function (x,y) {
  return this.found[y][x] === this.data.grid[y][x]
}

Game.prototype.isInvalid = function (x,y) {
  return this.isFilled(x,y) && !this.isValid(x,y)
}

Game.prototype.isFinished = function () {
  return this.validTiles == this.width * this.height
}

Game.prototype.getFillingProgress = function () {
  return this.filledTiles / (this.width * this.height)
}

Game.prototype.getHintsProgress = function () {
  var total = this.colHints.hintCount + this.rowHints.hintCount
  var valid = 0
  for (var c in this.colors) {
    valid += this.colHints.validCount(c)
    valid += this.rowHints.validCount(c)
  }
  return valid / total
}

Game.prototype.set = function (x,y,c) {
  this.filledTiles -= this.isFilled(x,y) ? 1 : 0
  this.validTiles -= this.isValid(x,y) ? 1 : 0
  this.found[y][x] = c
  this.filledTiles += this.isFilled(x,y) ? 1 : 0
  this.validTiles += this.isValid(x,y) ? 1 : 0
  this.validateHintsAt(x,y)
  if (this.isFinished())
    this.endTimer()
}

Game.prototype.erase = function (x,y) {
  this.set(x,y,false)
}

Game.prototype.setCross = function (x,y,c,state) {
  this.crosses[c].set(x,y,state)
  this.validateHintsAt(x,y)
}

Game.prototype.cross = function (x,y,c) {
  this.setCross(x,y,c,true)
}

Game.prototype.uncross = function (x,y,c) {
  this.setCross(x,y,c,false)
}


// --- Tile state extraction ---

/* In the following functions we abstract the state of tiles for a given color:
   - 'filled' if filled with the given color
   - 'crossed' if crossed for the given color or filled with another color
   - 'empty' otherwise */

Game.prototype.getState = function (x,y,c) {
  if (this.get(x,y) === c)
    return 'filled'
  else if (!this.isEmpty(x,y,c))
    return 'crossed'
  else
    return 'empty'
}

Game.prototype.setState = function (x,y,c,state) {
  if (state === 'filled')
    this.set(x,y,c)
  else if (state === 'crossed')
    this.cross(x,y,c)
  else if (state === 'empty') {
    this.set(x,y,false)
    this.uncross(x,y,c)
  }
}

Game.prototype.getColState = function(x,c)
{
  var line = []
  for (var y = 0; y < this.height; y++)
    line[y] = this.getState(x,y,c)
  return line
}

Game.prototype.getRowState = function(y,c)
{
  var line = []
  for (var x = 0; x < this.width; x++)
    line[x] = this.getState(x,y,c)
  return line
}


// --- Hints ---

Game.prototype.getRowHints = function (y,c) {
  return this.rowHints.get(c,y)
}

Game.prototype.getColHints = function (x,c) {
  return this.colHints.get(c,x)
}

Game.prototype.validateHints = function () {
  for (var c in this.colors) {
    for (var y = 0; y < this.height; y++)
      validateHints(this.getRowHints(y,c), this.getRowBlocks(y,c))
    for (var x = 0; x < this.width; x++)
      validateHints(this.getColHints(x,c), this.getColBlocks(x,c))
  }
}

Game.prototype.validateHintsAt = function (x, y) {
  for (var c in this.colors) {
    validateHints(this.getRowHints(y,c), this.getRowBlocks(y,c))
    validateHints(this.getColHints(x,c), this.getColBlocks(x,c))
  }
  this.validateColors()
}


// --- Colors ---

Game.prototype.validateColors = function () {
  for (var c in this.colors) {
    var valid = 'valid'
    valid = this.colHints.combineState(c, valid)
    valid = this.rowHints.combineState(c, valid)
    this.colors[c].valid = valid
  }
}


// --- Initialisation ---

Game.prototype.setup = function (data) {
  /* Init puzzle properties */

  this.data = data
  this.height = data.grid.length
  this.width = this.height > 0 ? data.grid[0].length : 0
  this.rowHints = new HintList()
  this.colHints = new HintList()

  /* Generating hints and colors */ 

  var hintCount = 0

  function generateHints(line, list, index) {
    var number = 0
    var current_color = 'undefined'
    for (var i = 0; i <= line.length; i++) {
      var color = line[i] ? line[i] : 'undefined'
      if (color == current_color) {
        // Same color, we simply increment the counter
        number++
      }
      else {
        // End of the color block, we register it in the hints
        if (current_color != 'undefined')
          list.add(current_color, index, number, hintCount++)
        number = 1
        current_color = color
      }
    }
  }

  var colors = {}

  function addColor(c) {
    if (!(c in colors))
      colors[c] = {color: c, valid: 'untouched'}
  }

  /* First for rows */
  for (var y = 0; y < this.height; y++) {
    var line = []
    for (var x = 0; x < this.width; x++) {
      line[x] = data.grid[y][x]
      addColor(line[x])
    }
    generateHints(line, this.rowHints, y)
  }

  /* Now for the columns */
  for (var x = 0; x < this.width; x++) {
    var line = []
    for (var y = 0; y < this.height; y++)
      line[y] = data.grid[y][x]
    generateHints(line, this.colHints, x)
  }

  this.colors = colors
}

Game.prototype.reset = function () {
  this.cheatCount = 0

  /* Generating the empty found grid */
  this.validTiles = 0
  this.filledTiles = 0
  this.found = []
  for (var y = 0; y < this.height; y++) {
    this.found[y] = []
    for (var x = 0; x < this.width; x++)
      this.found[y][x] = false
  }

  /* Generate the initial cross grid */
  var that = this
  this.crosses = {}
  for (var c in this.colors) {
    this.crosses[c] = new Bitarray2d(this.width, this.height, function (x, y) {
        return !that.isFillable(x, y, c)
      })
  }

  this.startDate = Date.now()
  this.elapsedTime = 0
  this.endDate = false
}


// --- Import / Export ---

Game.prototype.save = function() {
  var crossesPacked = {}
  for (var c in this.crosses)
    crossesPacked[c] = this.crosses[c].pack()

  return {
    cheatCount: this.cheatCount,
    found: this.found,
    crosses: crossesPacked,
    startDate: this.startDate,
    endDate: this.endDate,
    elapsedTime: this.elapsedTime
  }
}

Game.prototype.load = function (state) {
  this.cheatCount = state.cheatCount
  this.found = state.found
  this.crosses = {}
  for (var c in state.crosses)
    this.crosses[c] = new Bitarray2d(this.width, this.height, state.crosses[c])
  this.startDate = state.startDate
  this.endDate = state.endDate
  this.elapsedTime = state.elapsedTime ? state.elapsedTime : 0

  this.resume()
  this.validate()
  this.validateHints()
  this.validateColors()
}


// --- Blocks of contiguous states ---

function isEmptyBlock(block) {
  return block.state == 'empty';
}

function isFilledBlock(block) {
  return block.state == 'filled';
}

function extractBlocks(line) {
  var blocks = []

  // Extract blocks
  var state = 'none'
  var size = 0
  for (var i = 0; i <= line.length; i++) {
    if (line[i] == state) {
      size ++
    }
    else {
      if (size > 0)
        blocks.push({state: state, start: i - size, end: i - 1, size: size})
      size = 1
      state = line[i]
    }
  }

  // Mark wheter blocks are closed or not
  for (var i = 0; i < blocks.length ; i ++) {
    if (isFilledBlock(blocks[i])) {
      blocks[i].closed =
        (!blocks[i-1] || !isEmptyBlock(blocks[i-1])) &&
        (!blocks[i+1] || !isEmptyBlock(blocks[i+1]))
    }
  }

  return blocks;
}

Game.prototype.getRowBlocks = function (y,c) {
  return extractBlocks(this.getRowState(y,c))
}

Game.prototype.getColBlocks = function (x,c) {
  return extractBlocks(this.getColState(x,c))
}


// --- Validation and error removal ---

Game.prototype.validate = function() {
  this.validTiles = 0
  this.filledTiles = 0
  for (var x = 0; x < this.width; x++) {
    for (var y = 0; y < this.height; y++) {
      this.validTiles += this.isValid(x,y) ? 1 : 0  
      this.filledTiles += this.isFilled(x,y) ? 1 : 0  
    }
  }
}

Game.prototype.removeError = function() {
  this.cheatCount++
  for (var y = 0; y < this.height; y++) {
    for (var x = 0; x < this.width; x++) {
      if (this.isInvalid(x,y)) {
        this.erase(x,y)
        return {x: x, y: y}
      }
    }
  }
  return null
}


// --- Time handling ---

Game.prototype.isPaused = function () {
  return this.startDate === false
}

Game.prototype.getElapsedTime = function () {
  if (this.startDate === false || this.endDate !== false)
    return this.elapsedTime
  else
    return this.elapsedTime + Date.now() - this.startDate
}

Game.prototype.endTimer = function () {
  if (this.endDate === false) {
    this.endDate = Date.now()
    this.elapsedTime += Date.now() - this.startDate
  }
}

Game.prototype.pause = function () {
  if (this.startDate !== false && this.endDate === false)
    this.elapsedTime += Date.now() - this.startDate
  this.startDate = false
}

Game.prototype.resume = function () {
  if (this.startDate === false)
    this.startDate = Date.now()
}

Game.prototype.togglePause = function () {
  if (this.isPaused())
    this.resume()
  else
    this.pause()
}

