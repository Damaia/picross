function solveLine(hints, line, fill) {
  // --- Initialization ---
  // Initialize blocks data
  var searched_blocks = hints.map(function (hint) {
      return {
        size: hint.number,
        min: 0,
        max: line.length - 1
      }
    })
  // Extract free blocks (blocks without crosses)
  var free_line = line.map(function (state) {
      return state == 'filled' ? 'empty' : state
    })
  var free_blocks = extractBlocks(free_line).filter(isEmptyBlock)

  // --- Compute minimum position for each block ---
  // Try to put them left
  var i = 0, j = 0, pos = 0;
  while (i < searched_blocks.length && j < free_blocks.length) {
    var searched = searched_blocks[i], free = free_blocks[j]
    if (pos + searched.size > free.size) {
      j++
      pos = 0
    }
    else {
      searched.min = free.start + pos
      pos += searched.size + 1
      i++
    }
  }
  // Take into account tiles which are already filled
  for (var i = searched_blocks.length - 1, pos = line.length - 1; i >= 0; i--) {
    var block = searched_blocks[i]
    var filled_pos = line.lastIndexOf('filled', pos)
    if (filled_pos >= 0) {
      var crossed_pos = line.lastIndexOf('crossed', filled_pos)
      pos = Math.max(crossed_pos + 1, filled_pos - block.size + 1)
      block.min = Math.max(pos, block.min)
    }
    pos = block.min - 2
  }

  // --- Compute maximum position for each block (same as before, but inverted ---
  // Try to put the right
  var i = searched_blocks.length - 1, j = free_blocks.length  - 1, pos = 0
  while (i >= 0 && j >= 0) {
    var searched = searched_blocks[i], free = free_blocks[j]
    if (pos + searched.size > free.size) {
      j--
      pos = 0
    }
    else {
      searched.max = free.end - pos
      pos += searched.size + 1
      i--
    }
  }
  // Take into account tiles which are already filled
  for (var i = 0, pos = 0; i < searched_blocks.length; i++) {
    var block = searched_blocks[i]
    var filled_pos = line.indexOf('filled', pos)
    if (filled_pos >= 0) {
      var crossed_pos = line.indexOf('crossed', filled_pos)
      crossed_pos = crossed_pos >= 0 ? crossed_pos : line.length
      pos = Math.min(crossed_pos - 1, filled_pos + block.size - 1)
      block.max = Math.min(pos, block.max)
    }
    pos = block.max + 2
  }

  // --- Use min and max values to fill the line ---
  var complete = true
  // Mark free tiles 
  line = line.map(function (state) {
      return state == 'empty' ? 'free' : state
    })
  // Filling blocks with found information
  for (var i = 0; i < searched_blocks.length; i++) {
    var block = searched_blocks[i]
    // If solution found is invalid
    if (block.max - block.min < block.size - 1) {
      console.log(hints, searched_blocks)
      throw "no solution"
    }
    // Is the block complete ?
    if (block.max - block.min > block.size - 1)
      complete = false
    // Mark potential tiles as 'empty'
    for (var j = block.min; j <= block.max; j++)
      line[j] = line[j] == 'free' ? 'empty' : line[j]
    // Fill tiles we are sure of 
    for (var j = block.max - block.size + 1 ; j < block.min + block.size; j++)
      line[j] = 'filled' 
  }
  // Cross any free tile left 
  line = line.map(function (state) {
      return state == 'free' ? 'crossed' : state
    })

  return {
    line: line,
    complete: complete
  }
}


function* solver(game) {
  var progress
  var completed = {}
  for (var c in game.colors)
    completed[c] = {rows:[],cols:[]}

  do
  {
    yield true
    progress = false
    for (var c in game.colors) {
      for (var y = 0; y < game.height; y++) {
        if (!completed[c].rows[y]) {
          var r = solveLine(game.getRowHints(y,c), game.getRowState(y,c))
          completed[c].rows[y] = r.complete
          setRowState(y, c, r.line)
        }
      }
      for (var x = 0; x < game.width; x++) {
        if (!completed[c].cols[x]) {
          var r = solveLine(game.getColHints(x,c), game.getColState(x,c))
          completed[c].cols[x] = r.complete
          setColState(x, c, r.line)
        }
      }
      yield false
    }
  }
  while (progress)

  function setRowState(y, c, line) {
    for (var x = 0; x < line.length; x++) {
      if (game.getState(x,y,c) !== line[x]) {
        game.setState(x,y,c,line[x])
        progress = true
      }
    }
  }

  function setColState(x, c, line) {
    for (var y = 0; y < line.length; y++) {
      if (game.getState(x,y,c) !== line[y]) {
        game.setState(x,y,c,line[y])
        progress = true
      }
    }
  }
}

function* solve(game) {
  var iteration = 0
  output("Trying to solve puzzle...")
  for (var p of solver(game)) {
    if (p)
      output("Iteration", ++iteration)
    yield null
  }
  if (game.isFinished())
    output("Puzzle solved in", iteration, "iterations !")
  else
    output("Failed to solve puzzle.")
  return game.isFinished() ? iteration : -1
}
