/* HintList stores the hints information about the puzzle, including
   their size and if they have been validated or not. */

function HintList(that) {
  if (that) {
    for (var k in that)
      this[k] = that[k]
  }
  else {
    this.maxHints = 0
    this.hintCount = 0
  }
}

HintList.prototype.add = function(color, index, number, no) {
  if (!this[color])
    this[color] = []
  if (!this[color][index])
    this[color][index] = []
  var hint = {
    color: color,
    number: number,
    state: 'untouched',
    no: no
  }
  this[color][index].push(hint)
  this.maxHints = Math.max(this.maxHints, this[color][index].length)
  this.hintCount++
}

HintList.prototype.get = function(color, index) {
  if (this[color] && this[color][index])
    return this[color][index]
  else
    return []
}

HintList.prototype.combineState = function(color, state) {
  var hints = this[color]
  for (var i = 0; i < hints.length; i++) {
    for (var j = 0; hints[i] && hints[i][j]; j++) {
      var hintState = hints[i][j].state
      if (hintState == 'invalid')
        return 'invalid'
      else if (hintState == 'untouched' && state != 'invalid')
        state = 'untouched'
    }
  }
  return state
}

HintList.prototype.validCount = function(color) {
  var count = 0
  var hints = this[color]
  for (var i = 0; i < hints.length; i++) {
    for (var j = 0; hints[i] && hints[i][j]; j++) {
      var hintState = hints[i][j].state
      if (hintState == 'valid')
        count++
    }
  }
  return count
}


/* Hints are considered invalid when
   1/ there are more blocks than hints;
   2/ the line is completed and there less blocks than hints;
   3/ otherwise if we can match a hint and a block and
      a. either the block is closed (there are no empty tiles surronding the block) and it has wrong size
      b. or it is not closed but its size is already too large. */

function matchHints(hints, blocks) {
  var filled_blocks = blocks.filter(isFilledBlock)
  var completed = !blocks.some(isEmptyBlock)

  if (filled_blocks.length > hints.length ||
      completed && filled_blocks.length != hints.length)
  {
    /* If we have too many blocks */
    return false
  }
  else
  {
    var matched_blocks = []
    /* Else, we try to match hints and blocks */
    if (filled_blocks.length == hints.length) {
      /* When we have the same number of blocks and hints, we can match them
       pointwise. */
       matched_blocks = filled_blocks
    }
    else {
      /* Otherwise we match blocks at the begining of the line */
      for (var i = 0, j = 0; i < blocks.length; i++) {
        if (blocks[i].state == 'filled')
          matched_blocks[j++] = blocks[i]
        else if (blocks[i].state == 'empty')
          break;
      }
      var first_unmatched_hint = j, first_unmatched_block = i
      /* and blocks at the end of the line */
      for (var i = blocks.length - 1, j = hints.length - 1; i >= 0; i--) {
        if (blocks[i].state == 'filled')
          matched_blocks[j--] = blocks[i]
        else if (blocks[i].state == 'empty')
          break;
      }
      var last_unmatched_hint = j, last_unmatched_block = i
    }

    return matched_blocks;
  }
}

function validateHints(hints, blocks) {
  var matched_blocks = matchHints(hints, blocks)
  if (matched_blocks === false) {
    for (var i = 0; hints[i]; i++)
      hints[i].state = 'invalid'
  }
  else {
    /* Validate matched hints */
    for (var i = 0; i < hints.length;  i++) {
      if (!matched_blocks[i])
        hints[i].state = 'untouched'
      else if (matched_blocks[i].size == hints[i].number)
        hints[i].state = 'valid'
      else if (matched_blocks[i].size > hints[i].number || matched_blocks[i].closed)
        hints[i].state = 'invalid'
      else
        hints[i].state = 'untouched'
    }
  }
}

/* Find which hint is matched by the cell at given index */
function findMatchedHints(hints, blocks, index) {
  var matched_blocks = matchHints(hints, blocks)
  if (matched_blocks === false) {
    return false;
  }
  else {
    var l = 0, u = hints.length - 1;
    for (var i = 0; i < hints.length;  i++) {
      var block = matched_blocks[i]
      if (block) {
        if (index > block.end)
          l = Math.max(l, i + 1)
        else if (index >= block.start && index <= block.end)
          l = u = i;
        else if (index < block.start)
          u = Math.min(u, i - 1)
      }
    }
    if (u < l || u < 0 || l >= hints.length)
      return false
    else
      return {l:l, u:u}
  }
}

