/* The class Bitarray2d represents a two dimensionnal array of booleans and
   tries to do it with an efficient in memory representation. Space
   efficiency is important whenever these arrays are stored in the
   localStorage or history. The pack and unpack functions can be 
   used to further improve (x8) the compression level. */

function Bitarray2d(width, height, init) {
  this.width = width
  this.height = height
  // Init must either be a function, a packed string or nothing
  if (typeof init == 'function')
    this.init(init)
  else if (init)
    this.unpack(init)
  else
    // Create a string of '0's of length width * height
    this.data = intToBitStream(0, width * height + 1)
}

Bitarray2d.prototype._index = function (x,y) {
  x = Number(x), y = Number(y)
  if (x >= 0 && x < this.width && y >= 0 && y < this.height)
    return x + y * this.width
  else
    return -1
}

Bitarray2d.prototype.get = function (x, y) {
  return this.data.charAt(this._index(x,y)) == '1' ? true : false;
}

Bitarray2d.prototype.set = function (x, y, value) {
  var index = this._index(x,y)
  if (index != -1) { // If we are inside the array
    this.data =
      this.data.substr(0,index) +
      (value ? '1' : '0') +
      this.data.substr(index+1)
  }
}

Bitarray2d.prototype.init = function (f) {
  this.data = ''
  for (y = 0; y < this.height; y++) {
    for (x = 0; x < this.width; x++) {
      this.data += f(x,y) ? '1' : '0'
    }
  }
}

Bitarray2d.prototype.pack = function () {
  var chunks = this.data.match(/.{1,16}/g)
  var packed = ''
  for (var i = 0; i < chunks.length; i++)
    packed += String.fromCharCode(parseInt(chunks[i], 2))
  return packed
}

Bitarray2d.prototype.unpack = function (packed) {
  this.data = '';
  for (var i = 0; i < packed.length; i++)
    this.data += intToBitStream(packed.charCodeAt(i),16)
}

function intToBitStream(x, size)
{
  return (Array(size).join('0') + x.toString(2)).slice(-size)
}
